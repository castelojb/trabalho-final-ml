import numpy as np
import pandas as pd


def format_user_rating_matrix(df_users: pd.DataFrame) -> np.ndarray:
	"""
	Function to transform users' DataFrame into a format suitable for using the svd model

	Args:
		df_users: Dataframe movelens with information from user ratings

	Returns:
		np.ndarray: rating matrix where columns are users and rows are movies
	"""
	ratings_mat = np.ndarray(
		shape=(np.max(df_users.movie_id.values), np.max(df_users.user_id.values)),
		dtype=np.uint8
	)

	ratings_mat[df_users.movie_id.values - 1, df_users.user_id.values - 1] = df_users.rating.values

	return ratings_mat


def normalize(matrix: np.ndarray) -> np.ndarray:
	"""
	function to normalize a numpy matrix
	Args:
		matrix: a numpy array with numbers

	Returns:
		np.ndarray: normalized matrix
	"""

	matrix - np.asarray([(np.mean(matrix, 1))]).T
	return matrix


def top_cosine_similarity(v_svd: np.ndarray, movie_id: int, top_n=10) -> np.array:
	"""
	Function to find the top-n elements based on the V matrix of the SVD

	Args:
		v_svd: matrix obtained by svd decomposition
		movie_id: movie id reference to search for similars
		top_n: desired ranking size

	Returns:
		np.array: ordered array of relevance of films
	"""
	index = movie_id - 1 # Movie id starts from 1 in the dataset
	movie_row = v_svd[index, :]
	magnitude = np.sqrt(np.einsum('ij, ij -> i', v_svd, v_svd))
	similarity = np.dot(movie_row, v_svd.T) / (magnitude[index] * magnitude)
	sort_indexes = np.argsort(-similarity)
	return sort_indexes[:top_n]


def print_similar_movies(df_movies: pd.DataFrame, movie_id: int, top_indexes: np.array) -> None:
	"""
	Function to print the results of the process

	Args:
		df_movies: DataFrame with movie information
		movie_id: movie id that was used by svd
		top_indexes: array with the most relevant films for the user

	Returns:

	"""
	print('Recommendations for {0}: \n'.format(
		df_movies[df_movies.movie_id == movie_id].title.values[0])
	)
	for id_ in top_indexes + 1:
		print(df_movies[df_movies.movie_id == id_].title.values[0])
