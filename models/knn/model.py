import pandas as pd
import numpy as np


"""
    Module for calculating the knn algorithm for recommendation systems
"""

def sim_cosseno(u: np.array, v: np.array) -> float:
    """
    Calculates the cosine similarity

    Args:
        u: user
        v: user

    Returns:
        float: cosine similarity
    """
    return np.dot(u, v)/(np.linalg.norm(u)*np.linalg.norm(v))


def sim_pearson(u: np.array, v: np.array) -> float:
    """
    Calculates Pearson correlation coefficient

    Args:
        u: user
        v: user

    Returns:
        float: Pearson correlation coefficient

    """
    return np.corrcoef(u, v)[0, 1]

def matriz_similaridade(train: np.array, test: np.array, tipo: str) -> np.array:
    """
    User similarity calculation

    Args:
        train: Train user matrix
        test: Test user matrix
        tipo: Similarity type, can be: "cosine" or "pearson"

    Returns:
        np.array: Similarity matrix of all test users to training users

    """
    similaridade = []
    
    if tipo == "cosseno":
        sim = sim_cosseno

    elif tipo == "pearson":
        sim = sim_pearson
    
    for user_test in test:
        user_sim_vec = []

        for user_train in train:
            user_sim_vec.append(sim(user_test, user_train))
        
        similaridade.append(user_sim_vec)
    return np.array(similaridade)

def gerar_vizinhos(matriz_similaridade: np.array) -> np.array:
    """
    Generates a matrix of neighbors ordered by similarity

    Args:
        matriz_similaridade: Similarity matrix

    Returns:
        np.array: Neighbors

    """
    vizinhos = []
    
    for user in matriz_similaridade:
        vizinhos.append(np.flip(np.argsort(user[user > 0])))
    
    return np.array(vizinhos)


def basic(u: np.array, vizinho_data: np.array, similaridade: np.array, k: np.array) -> np.array:
    """
    Calculation of grade prediction for all items using weighted average

    Args:
        u: User
        vizinho_data: Neighbors dados
        similaridade: Similarity matrix
        k: Number of neighbors

    Returns:
        np.array: Predicted Notes
    """

    predict = []
    soma = 0
    div = 0
    for filme in range(u.shape[0]):
        
        k_cont = 0
        
        for v, s in zip(vizinho_data[:, filme], similaridade):
            if v != 0:
                soma += v * s
                div += np.abs(s)
                k_cont += 1
                
                if k == k_cont:
                    break
        predict.append(soma/div if div != 0 else 0)
    
    return predict

def withMeans(u: np.array, vizinho_data: np.array, similaridade: np.array, k: np.array) -> np.array:
    """
    Calculation of grade prediction for all items using weighted average with mean-centered normalization

    Args:
        u: User
        vizinho_data: Neighbors dados
        similaridade: Similarity matrix
        k: Number of neighbors

    Returns:
        np.array: Predicted Notes
    """

    predict = []
    soma = 0
    div = 0
    
    medias = []
    media_u = np.average(u[u != 0])
    
    for viz in vizinho_data:
        viz = viz[viz != 0]
        medias.append(np.average(viz))
    
    for filme in range(u.shape[0]):
        
        k_cont = 0
        
        for v, m,  s in zip(vizinho_data[:, filme], medias, similaridade):
            if v != 0:
                soma +=  s * (v - m)
                div += np.abs(s)
                k_cont += 1
                
                if k == k_cont:
                    break
        predict.append(media_u + soma/div if div != 0 else 0)
    
    return predict
    
def withZScore(u: np.array, vizinho_data: np.array, similaridade: np.array, k: np.array) -> np.array:
    """
    Calculation of grade prediction for all items using weighted average with Z-Score normalization

    Args:
        u: User
        vizinho_data: Neighbors dados
        similaridade: Similarity matrix
        k: Number of neighbors

    Returns:
        np.array: Predicted Notes
    """

    predict = []
    soma = 0
    div = 0
    
    medias = []
    desvios = []

    
    media_u = np.average(u[u != 0])
    desvio_u = np.std(u[u != 0])

    
    for viz in vizinho_data:
        viz = viz[viz != 0]
        medias.append(np.average(viz))
        desvios.append(np.std(viz))

    
    for filme in range(u.shape[0]):
        k_cont = 0
        for v, m, d,  s in zip(vizinho_data[:, filme], medias, desvios, similaridade):
            if v != 0:
                soma +=  s * (v - m) / d
                div += np.abs(s)
                k_cont += 1
                
                if k == k_cont:
                    break
        
        predict.append(media_u + desvio_u * soma/div if div != 0 else 0)
    
    return predict




class kNN():
    parametros = {
        "similaridade": "cosseno",
        "predicao": "basic",
        "k": 40, 
        "n_test": 10
    }
    def __init__(self, parametros=dict()):
        """
        Args:
            parametros: Dictionary with attributes similaridade, predicao, n_test
        """
        for par in parametros:
            self.parametros[par] = parametros[par]

    def gerar_similiridade(self, matrix: np.ndarray):
        
        self.row_data_test = matrix.sample(n=self.parametros["n_test"], random_state=0)
        self.row_data_train = matrix.drop(self.row_data_test.index)

        self.data_train = np.array(self.row_data_train)
        self.data_test = np.array(self.row_data_test)
        
        self.matriz_similaridade = matriz_similaridade(
            self.data_train, 
            self.data_test, 
            self.parametros["similaridade"]
        )


    def train(self, matrix: np.ndarray) -> bool:
        self.vizinhos = gerar_vizinhos(self.matriz_similaridade)


    def predict_top_n(self, n: int) -> list:

        if self.parametros["predicao"] == "basic":
            predict_function = basic

        if self.parametros["predicao"] == "withMeans":
            predict_function = withMeans
            
        if self.parametros["predicao"] == "withZScore":
            predict_function = withZScore

        predictions = []

        for user, vizinho, similaridade in zip(self.data_test, self.vizinhos, self.matriz_similaridade):
            
            vizinho_data = self.data_train[vizinho]
            sim_vizinho = similaridade[vizinho]

            predictions.append(predict_function(user, vizinho_data, sim_vizinho, self.parametros["k"]))
            
        self.predicao = np.array(predictions)
        
        return self.top_n(n)
    
    def top_n(self, n):
        top = []
        for pred in self.predicao:
            top.append(np.flip(np.argsort(pred)[-n:]))
        
        self.top_n_dic = dict()
        for index, userId in enumerate(self.row_data_test.index):
            top_film = []
            for pred in top[index]:
                top_film.append(self.row_data_test.columns[pred])
                
            self.top_n_dic[userId] = top_film
        
        
        return self.top_n_dic


    def mae(self):
        soma = 0
        n = 0
        for user in range(self.data_test.shape[0]):
            for u, u_p in zip(self.data_test[user], self.predicao[user]):
                if u != 0:
                    soma += np.abs(u - u_p)
                    n += 1
        return soma/n

    def rmse(self):
        soma = 0
        n = 0
        for user in range(self.data_test.shape[0]):
            for u, u_p in zip(self.data_test[user], self.predicao[user]):
                if u != 0:
                    soma += (u - u_p)**2
                    n += 1
        return (soma/n)**(1/2)


