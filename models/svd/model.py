import numpy as np
from numpy.linalg import norm
from models.model_interface import ModelTopN


def response_kick(n: int) -> np.array:
    """
    guess a possible normalized response
    Args:
        n: vector length

    Returns:
        np.array: vector of length n with normalized and random numbers
    """
    vector = np.random.rand(n)

    return vector / norm(vector)


def power_method(M: np.ndarray, epsilon=1e-10) -> np.array:
    """
    power method to find an eigenvector of the matrix
    Args:
        M: Matrix to be used
        epsilon: stop criterion

    Returns:
        np.array: M matrix eigenvector
    """

    n, m = M.shape

    x = response_kick(min(n, m))

    v = x

    # solve Mt . M
    if n > m:
        B = np.dot(M.T, M)

    # solve M . Mt
    else:
        B = np.dot(M, M.T)

    while True:

        previous_v = v

        # correcting the answer
        v = np.dot(B, previous_v)
        v = v / norm(v)

        # checking whether the vector is orthogonal
        if abs(np.dot(v, previous_v)) > 1 - epsilon:
            return v


def _svd(M: np.ndarray, k=None, epsilon=1e-10) -> (np.ndarray, np.ndarray, np.ndarray):
    """
    Calculating the svd using the power method
    M = U . S . Vt
    Args:
        M: Matrix that will be factored
        k: number of eigenvalues to be computed
        epsilon: the precision of the power method

    Returns:
        (U, S, V): the factored matrix
    """
    M = np.array(M, dtype=float)

    n, m = M.shape

    if k is None:
        k = min(n, m)

    u_s_v_vectors = np.ndarray(k, dtype=np.object)

    for i in range(k):
        matrixFor1D = M.copy()

        # reducing the eigenvalues and eigenvectors found

        for u, s, v in u_s_v_vectors[:i]:
            matrixFor1D -= s * np.outer(u, v)

        # solve Mt . M and find V
        if n > m:

            # finding an eigenvector v
            v = power_method(matrixFor1D, epsilon=epsilon)

            # computing u norm != 1
            u_ = M.dot(v)

            # finding the singular value
            s = norm(u_)

            # computing eigenvector u
            u = u_ / s

        # solve M . Mt and find U
        else:

            # finding an eigenvector u
            u = power_method(matrixFor1D, epsilon=epsilon)

            # computing v norm != 1
            v_ = M.T.dot(u)

            # finding the singular value
            s = norm(v_)  # next singular value

            # computing eigenvector v
            v = v_ / s


        u_s_v_vectors[i] = (u, s, v)

    us, s, vs = [np.array(x) for x in zip(*u_s_v_vectors)]

    return us.T, s, vs


class SVD(ModelTopN):

    def __init__(self):
        pass

    def _format_s_matrix(self, s):

        form = np.ndarray([s.shape[0], s.shape[0]])

        for index, value in enumerate(s):

            form[index, index] = value

        return form

    def train(self, matrix: np.ndarray) -> bool:

        self.m = matrix

        try:
            self.u, self.s, self.v = _svd(matrix)
            
            self.s = self._format_s_matrix(self.s)

            self.pu = self.u.dot(np.sqrt(np.abs(self.s)))

            self.qi = self.v.T.dot(np.sqrt(np.abs(self.s)))

            return True
        except Exception as e:

            print(e)

            return False

    def predict(self, user_id: int, movie_id: int) -> float:

        return np.inner(self.qi[movie_id], self.pu[user_id])



    def test(self, df_,col_user='formated_user_id', col_prod='formated_movie_id') -> list:

        preds = []

        df = df_[[col_user, col_prod]]

        for user in df[col_user].unique():

            films = df[df[col_user] == user][col_prod].unique()

            for prod in df[~df[col_prod].isin(films)][col_prod].unique():

                est = self.predict(user, prod)

                preds.append(dict(uid=str(user), iid=str(prod), est=est))

        return preds

    def predict_top_n(self, predictions, n: int) -> list:

        users = dict()

        for pred in predictions:

            id_user, id_item, est = pred['uid'], pred['iid'], pred['est']

            if not users.get(id_user):
                users[id_user] = []

            users[id_user].append((id_item, est))

        for key, value in users.items():
            top_n = np.array(sorted(value, key=lambda x: x[1], reverse=True)[:n])

            users[key] = top_n

        return users

