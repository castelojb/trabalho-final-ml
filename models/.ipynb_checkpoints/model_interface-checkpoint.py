import abc
import numpy as np

"""
module to elaborate a prediction model
"""


class ModelTopN(abc.ABC):

	@abc.abstractmethod
	def train(self, matrix: np.ndarray) -> bool:
		"""
		function to train a model based on a numpy matrix
		Args:
			matrix: numerical matrix to use the model

		Returns:
			bool: True if the training was successful, False otherwise
		"""
		pass

	@abc.abstractmethod
	def predict_top_n(self, value, n: int) -> list:
		"""
		top n model prediction
		Args:
			value: model input
			n: top n recommendation

		Returns:
			list: list of recommendations
		"""
		pass
