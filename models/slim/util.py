import pandas as pd
import numpy as np
import math
from sklearn.model_selection import train_test_split

def treat_data(df, labels=['user_id', 'movie_id', 'rating']):
    uid_label, iid_label, _ = labels
    count_by_movie = df.groupby(by=iid_label).apply(lambda g: g.count())
    movies_to_drop = count_by_movie[count_by_movie[uid_label] < 10].index.tolist()
    return df[~df[iid_label].isin(movies_to_drop)]

def split_rating_train_test(df, train_size=0.75, labels=['user_id', 'movie_id', 'rating']):
    uid_label, _, _ = labels
    ratings_by_user = { user_id: [] for user_id in df[uid_label].unique() }
    for row in df.itertuples():
        user_id = getattr(row, uid_label)
        ratings_by_user[user_id].append(row.Index)
    train_indexes = []
    test_indexes = []
    for user_id in ratings_by_user:
        user_train, user_test = train_test_split(ratings_by_user[user_id], train_size=train_size, shuffle=True)
        train_indexes += user_train
        test_indexes += user_test
    return df.loc[train_indexes], df.loc[test_indexes]