import pandas as pd
import numpy as np
from SLIM import SLIM, SLIMatrix

def slim_loocv(df_train):
    topn_predicted = {}
    left_out_predictions = []
    movies_ids = df_train[1].unique()
    users_ids = df_train[0].unique()
    count = 0
    for user_id in users_ids:
        if count % 10 == 0:
            print("%2.2f%%" % ((count / len(users_ids)) * 100))
        max_rating = df_train[df_train[0] == user_id][2].max()
        user_rat_index = df_train.index[(df_train[0] == user_id) & (df_train[2] == max_rating)][0]
        user_rat = df_train.loc[user_rat_index]
        train_set_df = df_train.copy().drop(user_rat_index)
        model = SLIM()
        trainmat = SLIMatrix(train_set_df)
        model.train(params, trainmat)
        testmat = SLIMatrix(df_train)
        predicted = model.predict(testmat, nrcmds=10)
        topn_predicted[user_id] = predicted[user_id]
        left_out_predictions.append(user_rat.to_numpy())
        count += 1
    
    return topn_predicted, left_out_predictions