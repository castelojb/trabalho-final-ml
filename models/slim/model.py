
import pandas as pd
import numpy as np
import random
import math
import multiprocessing
import functools
from sklearn.metrics.pairwise import cosine_similarity
from scipy.sparse import lil_matrix
from sklearn.linear_model import SGDRegressor
import numpy as np
from .model.model_interface import ModelTopN

class SLIM(ModelTopN):
    def __init__(self,
                 l1_reg= 0.001,
                 l2_reg= 0.0002,
                 fs_k=200,
                 labels=['user_id', 'movie_id', 'rating'],
                 verbose = False):
        alpha = l1_reg + l2_reg
        l1_ratio = l1_reg / alpha
        
        self.verbose = verbose
        
        self.uid_label = labels[0]
        self.mid_label = labels[1]
        self.rat_label = labels[2]
        
        self.model = SGDRegressor(
            penalty='elasticnet',
            fit_intercept=False,
            alpha=alpha,
            l1_ratio=l1_ratio,
        )

        self.coef_ = []
        self.train_dataset = None

        self.fs_k = fs_k

    def _calc_columns_weights(self, j: int) -> np.array:
        A = self.train_dataset.to_numpy()
        n = A.shape[1]

        aj = A[:, j].copy()

        A[:, j] = 0

        d = cosine_similarity(A.T, [aj]).flatten()

        most_similar_columns = np.argsort(d)[-1:-self.fs_k-1:-1]

        self.model.fit(A[:, most_similar_columns], aj.ravel())

        if j % 50 == 0 and self.verbose:
            print('-> %2.2f%%' % ((j / float(n)) * 100))

        # We need to reinstate the matrix
        A[:, j] = aj

        w = np.zeros(n)
        w[most_similar_columns] = self.model.coef_

        # Removing negative values because it makes no sense in our approach
        w[w < 0] = 0

        return w
        
    def _treat_and_pivot_data(self, df: pd.DataFrame) -> pd.DataFrame:
        df[self.rat_label] = df[self.rat_label] / df[self.rat_label].max()
        df = df.pivot(index=self.uid_label, columns=self.mid_label, values=self.rat_label) \
               .replace(to_replace=np.nan, value=0)
        return df
    
    def train(self, train_dataset: pd.DataFrame):
        self.train_dataset = self._treat_and_pivot_data(train_dataset)
        self.movies_ids = self.train_dataset.columns.tolist()
        self.users_ids = self.train_dataset.index.tolist()
        
        A = self.train_dataset.to_numpy()
        m, n = A.shape

        # Fit each column of W separately
        W = lil_matrix((n, n))

        pool = multiprocessing.Pool()
        weights = pool.map(self._calc_columns_weights, range(n))

        for j, w in enumerate(weights):
            for el in w.nonzero()[0]:
                W[(el, j)] = w[el]

        self.coef_ = W

    def predict_top_n(self, test_dataset: pd.DataFrame, n_rank: int = 10) -> list:
        """
        Generate the A_hat recommendations matrix
        """
        self.test_dataset = self._treat_and_pivot_data(test_dataset)
        if self.test_dataset.shape[1] > self.train_dataset.shape[1]:
            diff = set(self.test_dataset.columns) - set(self.train_dataset.columns)
            self.test_dataset = self.test_dataset.drop(columns=list(diff))
        elif self.test_dataset.shape[1] < self.train_dataset.shape[1]:
            diff = set(self.train_dataset.columns) - set(self.test_dataset.columns)
            for new_col in diff:
                self.test_dataset[new_col] = np.zeros(self.test_dataset.shape[0])
        A = self.test_dataset.to_numpy()
        A_hat = A * self.coef_

        recommendations = {}
        m, n = A.shape

        # Organizing A_hat matrix to simplify Top-N recommendation
        for u in range(0, m):
            user_id = self.users_ids[u]
            for i in range(0, n):
                v = A_hat[u, i]
                if v > 0:
                    if A[u, i] == 0:
                        if user_id not in recommendations:
                            recommendations[user_id] = [(self.movies_ids[i], v)]
                        else:
                            recommendations[user_id].append((self.movies_ids[i], v))

        # Ordering the most probable recommendations by A_hat
        for u in list(recommendations.keys()):
            top_n = sorted(recommendations[u], reverse=True, key=lambda x: x[1])[:n_rank]
            recommendations[u] = [movie_rat[0] for movie_rat in top_n]

        return recommendations
    
    def get_watched_movies(self, df_ratings: pd.DataFrame, min_rat = 0):
        rat_by_user_df = df_ratings.groupby(self.uid_label)
        rat_by_user_dict = {}
        for user_id, user_rats in rat_by_user_df:
            user_rats = user_rats.sort_values(by=[self.rat_label], ascending=False)
            rat_by_user_dict[user_id] = user_rats[user_rats[self.rat_label] >= min_rat][self.mid_label].tolist()
        return rat_by_user_dict
        