def HitRate(topNPredicted, leftOutPredictions):
    hits = 0
    total = 0

    # For each left-out rating
    for leftOut in leftOutPredictions:
        userID = leftOut[0]
        
        leftOutMovieID = leftOut[1]
        # Is it in the predicted top 10 for this user?
        hit = False
        if leftOutMovieID in topNPredicted[userID]:
            hits += 1

        total += 1

    # Compute overall precision
    return hits/total
    
def CumulativeHitRate(topNPredicted, leftOutPredictions, ratingCutoff=0):
    hits = 0
    total = 0
    # For each left-out rating
    for pred in leftOutPredictions:
        userID = pred[0]
        leftOutMovieID = pred[1]
        actualRating = df_train[(df_train[0] == userID) & (df_train[1] == leftOutMovieID)].iloc[0][2]
        # Only look at ability to recommend things the users actually liked...
        if (actualRating >= ratingCutoff):
            # Is it in the predicted top 10 for this user?
            hit = False
            for movieID in topNPredicted[userID]:
                if (leftOutMovieID == movieID):
                    hit = True
                    break
            if (hit) :
                hits += 1
            total += 1

        # Compute overall precision
    return hits/total

# Compute ARHR
def AverageReciprocalHitRank(topNPredicted, leftOutPredictions):
    summation = 0
    total = 0
    # For each left-out rating
    for pred in leftOutPredictions:
        userID = pred[0]
        leftOutMovieID = pred[1]
        # Is it in the predicted top N for this user?
        hitRank = 0
        rank = 0
        for movieID in topNPredicted[userID]:
            rank = rank + 1
            if (leftOutMovieID == movieID):
                
                hitRank = rank
                break
        if (hitRank > 0) :
            summation += 1.0 / hitRank
        print(hitRank)

        total += 1

    return summation / total

def ItemCoverage(topNPredicted, numItems):
    movies = set()
    for userID in topNPredicted.keys():
        movies = movies | set(topNPredicted[userID])
    return len(movies) / numItems

def getPopularityRanks(df_ratings):
    ratings = df.groupby(1).agg({ 0: 'count' }).to_dict()[0]
    rankings = {}
    rank = 1
    for movieID, ratingCount in sorted(ratings.items(), key=lambda x: x[1], reverse=True):
        rankings[movieID] = rank
        rank += 1
    return rankings

def Novelty(topNPredicted, rankings):
    n = 0
    total = 0
    for userID in topNPredicted.keys():
        for movieID in topNPredicted[userID]:
            rank = rankings[movieID]
            total += rank
            n += 1
    return total / n