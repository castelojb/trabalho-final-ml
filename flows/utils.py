import numpy as np
import pandas as pd


def read_movelens_dataset(path: str) -> (pd.DataFrame, pd.DataFrame):
	"""
	function to automate the reading of movelens data
	Args:
		path: way to movelens directory, EX: 'data/ml-1m'

	Returns:
		(movies, users): a tuple with the two DataFrames
	"""
	dtype = {'rating': np.int32,
			 'time': np.int32,
			 'user_id': np.int32,
			 'movie_id':np.int32}

	df_users = pd.read_csv(f'{path}/ratings.dat',
						   names=['user_id', 'movie_id', 'rating', 'time'],
						   delimiter='::',
						   dtype=dtype,
						   engine='python')

	df_movies = pd.read_csv(f'{path}/movies.dat',
							names=['movie_id', 'title', 'genre'],
							delimiter='::',
							engine='python')

	return df_movies, df_users


def cleaning_users(df_: pd.DataFrame, count_of_ratings=10) -> pd.DataFrame:
	"""
	function to clear user data
	Args:
		df_: DataFrame of movelens users
		count_of_ratings: count of minimum ratings for selection

	Returns:
		pd.DataFrame: Users DataFrame with data cleaning
	"""

	df = df_.reset_index(drop=True)

	# now the sequence of ids starts from 0
	df['user_id'] = df['user_id'] - 1
	df['movie_id'] = df['movie_id'] - 1

	# selection of movies who rated more than count_of_ratings users
	df_gb = df.groupby(by='movie_id') \
		.agg({'movie_id': 'count'}) \
		.query(f'movie_id >= {count_of_ratings}')

	filter_ = df['movie_id'].isin(df_gb.index)

	return df[filter_]


def normalize_dataframe_column(df_: pd.DataFrame, col_name: str) -> pd.DataFrame:
	"""
	function to normalize the values of a column in a DataFrame
	Normalization: (Col - Mean) / standard deviation
	Args:
		df_: original pd.DataFrame
		col_name: column name, or list of names, that will be normalized

	Returns:
		pd.DataFrame: DataFrame with the addition of normalized columns

	"""

	df = df_.reset_index(drop=True)

	if isinstance(col_name, str):
		col_name = [col_name]

	for name in col_name:
		df[f'{name}_normalized'] = (df[name] - df[name].mean()) / df[name].std()

	return df


def create_matrix_from_df(df_: pd.DataFrame, index_name: str, col_name: str, value_name: str,
						replace_value=0) -> np.ndarray:
	"""
	function to create a numpy matrix from a pd.DataFrame
	Args:
		df_: Original pd.DataFrame
		index_name: column name that will be the matrix index
		col_name: column name that will be the matrix columns
		value_name: column name that will be the matrix values
		replace_value: value that will replace null pd.DataFrame values

	Returns:
		np.ndarray: numpy matrix extracted from the pd.DataFrame
	"""
	df = df_.reset_index(drop=True)

	return df.pivot(index=index_name, columns=col_name, values=value_name) \
		.replace(to_replace=np.nan, value=replace_value) \
		.to_numpy()


def split_train_test(df_: pd.DataFrame, train_percent: int, shuffle=False) -> (pd.DataFrame, pd.DataFrame):
	"""
	Separation test and training of a DataFrame
	Args:
		df_: Original DataFrame
		train_percent: [0, 1] int interval
		shuffle: whether data should be mixed

	Returns:
		(train, test): a tuple with the training and test DataFrames
	"""
	if shuffle:
		msk = np.random.rand(df_.shape[0]) < train_percent

	else:
		arr = np.linspace(0, 1, num=df_.shape[0])
		msk = arr < train_percent

	train = df_.iloc[msk]

	test = df_.iloc[~msk]

	return train, test


def split_train_test_users_by_time(df_: pd.DataFrame, train_percent) -> (pd.DataFrame, pd.DataFrame):
	"""
	Function to separate training and test sets based on users time
	Args:
		df_: Original DataFrame
		train_percent: [0, 1] int interval

	Returns:
		(train, test): a tuple with the training and test DataFrames

	"""

	df = df_.reset_index(drop=True).sort_values(['user_id', 'time'])

	df_gb = df.groupby('user_id')

	train_indexes = []

	test_indexes = []

	for user_id, group in df_gb:

		df_train, df_test = split_train_test(group, train_percent)

		train_indexes.extend(df_train.index.tolist())

		test_indexes.extend(df_test.index.tolist())

	return df.loc[train_indexes], df.loc[test_indexes]








